/**
 * 执行命令
 * @param cmd string
 * @returns {Promise<string>}
 */
const getStdout = (cmd) => {
    return new Promise((resolve) => {
        const exec = require('child_process').exec;
        exec(cmd, function (err, stdout, stderr) {
            if (err) {
                console.log('get stdout error:' + cmd);
                console.log(stderr);
                resolve('');
            } else {
                resolve(stdout.replace(/^[\s\n\r]+|[\s\n\r]+$/, ''));
            }
        });
    });
};

/**
 * 检查提交状态
 */

function checkCommitStatus() {
    getStdout('git status').then((status) => {
        if (status.indexOf('Changes') > 0) {
            throw new Error('请前提交代码以做版本发布追踪');
        }
    });
}

module.exports = {
    checkCommitStatus
}

