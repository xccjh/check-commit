## 检查代码是否提交入库，用来选择终止脚本继续运行

### 实现效果

![实现效果](break.png)

### 安装

```
npm i -D @xccjh/check-commit
# or
yarn add -D @xccjh/check-commit
```



### 使用

- 新增checkCommit.js
```js
const {checkCommitStatus} = require('@xccjh/check-commit');
checkCommitStatus();
```

- package.json
```shell script
   "oss:test": "node checkCommit.js && npm run build:test && npm run uploadOssTest && npm run uploadOssTestBak",
```

运行`npm run oss:test`如果代码没有入库提交阻断脚本继续运行




